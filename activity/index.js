const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFirstName = document.querySelector('#span-first-name');
const spanLastName = document.querySelector('#span-last-name');

txtFirstName.addEventListener('keyup', (event)=>{
	spanFirstName.innerHTML = txtFirstName.value+" ";
});

txtLastName.addEventListener('keyup', (event)=>{
	spanLastName.innerHTML = txtLastName.value;
});